const characterId = location.pathname.split("/").pop();

const buttonHtml = `<a
  download="${characterId + ".json"}"
  class="user-interactions-quick-link user-interactions-quick-download"
  title="Download data as json"
  href="${location.pathname + "/json"}">
  <i class="fa fa-download"></i>
  </a>`;

const container = document.querySelector(".user-interactions-quick");
container.insertAdjacentHTML("beforeend", buttonHtml);
